<?php

namespace App\Http\Controllers;

use App\Models\CoverPage;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use SebastianBergmann\CodeCoverage\Report\Xml\Coverage;

class CoverPageController extends Controller
{
    public function index($user_id = null)
    {
        $datas = CoverPage::where('user_id', $user_id)->get();

        return json_encode($datas);
    }

    public function create($cover_page_id = null)
    {
        $datas = CoverPage::where('id', $cover_page_id)->get();

        if ($datas) {
            return json_encode([
                "data"   => json_encode($datas),
                "message"   => 'Successfully data retrived',
                'status' => 200
            ]);
        }
        return json_encode([
            "data"   => 'Not Found',
            'status' => 400
        ]);

        return json_encode($datas);
    }

    public function store(Request $request)
    { 
        
        // Move the uploaded image to the desired folder (e.g., public/uploads)
        $rules = [
            'university_name'               => 'string|nullable',
            'logo'                          => 'file|nullable|mimes:jpeg,png,jpg,gif,svg',
            'course_title'                  => 'string|nullable',
            'course_code'                   => 'string|nullable',
            'course_teacher_name'           => 'string|nullable',
            'course_teacher_designation'    => 'string|nullable',
            'name'                          => 'string|nullable',
            'student_id'                    => 'string|nullable',
            'batch'                         => 'string|nullable',
            'program'                       => 'string|nullable',
            'submission_date'               => 'string|nullable',
        ];

        $request->validate($rules);

        // $base64Image = base64_encode(File::get($request->logo));
        try {
            if ($request->id != 'undefined') {
                $coverPage                  =  CoverPage::find($request->id);
                $coverPage->logo = $coverPage->logo;
            } else {
                $coverPage = new CoverPage();
            }
            if ($request->hasFile('logo')) {
                $logoName = time().'.'.$request->logo->extension();  
     
                $request->logo->move(public_path('images'), $logoName);
                
                $coverPage->logo = '/images/'.$logoName;
                
            }
            $coverPage->user_id                     = $request->userId;
            $coverPage->template_id                 = $request->templateId;
            $coverPage->university_name             = $request->university_name;
            $coverPage->course_title                = $request->course_title;
            $coverPage->course_code                 = $request->course_code;
            $coverPage->course_teacher_name         = $request->course_teacher_name;
            $coverPage->course_teacher_designation  = $request->course_teacher_designation;
            $coverPage->name                        = $request->name;
            $coverPage->student_id                  = $request->student_id;
            $coverPage->batch                       = $request->batch;
            $coverPage->program                     = $request->program;
            $coverPage->submission_date             = $request->submission_date;

            if ($coverPage->save()) {
                return json_encode([
                    "data"   => 'Successfully data saved',
                    'status' => 200
                ]);
            }
            return json_encode([
                "data"   => 'Data not saved',
                'status' => 400
            ]);
        } catch (Exception $e) {
            return json_encode([
                'message' => $e->getMessage()
            ]);
        }
    }

    public function destroy(Request $request, $coverPageId)
    {
        $coverPage = CoverPage::find($coverPageId);

        // Check if the cover page exists
        if (!$coverPage) {
            return response()->json(
                [
                    'message' => 'Cover page not found'
                ],
                404
            );
        }

        // Delete the cover page
        $coverPage->delete();

        return response()->json(
            [
                'message' => 'Cover page deleted',
                'status'  => 200
            ],
            200
        );
    }

    public function statusUpdate($coverPageId = null, Request $request)
    {
        if (CoverPage::where('id', $coverPageId)->update(['status' => $request->status])) {
            return json_encode([
                "data" => 'Successfully data Upadated',
                'status' => 200
            ]);
        } else {
            return json_encode([
                "data" => 'Sorry data not Upadate',
                'status' => 400
            ]);
        }
    }
}
