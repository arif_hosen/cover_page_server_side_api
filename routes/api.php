<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\CoverPageController;
use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// authentication
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login'])->name('login');
Route::post('/logout', [AuthController::class, 'logout'])->middleware('auth:sanctum');
Route::post('/forget-password', [AuthController::class, 'forgetPassword'])->middleware('guest')->name('password.email');
Route::get('/reset-password/{token}', function (string $token) {
    return view('auth.reset-password', ['token' => $token]);
})->middleware('guest')->name('password.reset');

Route::post('/reset-password', function (Request $request) {
    $request->validate([
        'token' => 'required',
        'email' => 'required|email',
        'password' => 'required|min:8',
    ]);

    $status = Password::reset(
        $request->only('email', 'password', 'token'),
        function (User $user, string $password) {
            $user->forceFill([
                'password' => Hash::make($password)
            ])->setRememberToken(Str::random(60));

            $user->save();

            event(new PasswordReset($user));
        }
    );

    return $status === Password::PASSWORD_RESET
        ? response()->json(
            [
                'message' => 'Successfully password reset!',
                'status' => 200
            ],
            201
        )
        : response()->json(
            [
                'message' => 'Something went wrong. Please try again!',
                'status' => 500
            ],
            201
        );
})->middleware('guest')->name('password.update');


Route::middleware(['cors', 'auth:sanctum'])->group(function () {
    Route::get('/cover-pages/{user_id}', [CoverPageController::class, 'index']);
    Route::post('/cover-page-data', [CoverPageController::class, 'store']);
    Route::post('/cover-pages/{coverPageId}/status', [CoverPageController::class, 'statusUpdate']);
    Route::delete('/cover-pages/{coverPageId}', [CoverPageController::class, 'destroy']);

    //  specific cover page data
    Route::get('/cover-page/{cover_page_id}', [CoverPageController::class, 'create']);
});
