<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CoverPage extends Model
{
    use HasFactory;

    protected $table = 'cover_pages';
    protected $fillable = [
        'template_id',
        'user_id',
        'university_name',
        'logo',
        'course_title',
        'course_code',
        'course_teacher_name',
        'course_teacher_designation',
        'name',
        'student_id',
        'batch',
        'program',
        'submission_date',
        'status'
    ];
}
