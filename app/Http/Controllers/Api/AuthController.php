<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Str;
use Illuminate\Auth\Events\PasswordReset;

class AuthController extends Controller
{

    public function register(Request $request)
    {

        // return "success";
        try {
            $rules = [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|unique:users|max:255',
                'password' => 'required|string|min:6',
            ];
            $request->validate($rules);
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);

            $token = $user->createToken('auth_token')->plainTextToken;

            return response()->json(
                [
                    'user' => $user,
                    'message' => 'User Created Successfully!',
                    'token' => $token,
                    'status' => 200
                ],
                201
            );
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 500,
                'message' => 'The email address already taken!'
            ], 500);
        }
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        try {
            if (!Auth::attempt($request->only('email', 'password'))) {
                throw ValidationException::withMessages([
                    'email' => 'Invalid credentials',
                ]);
            }

            $user = $request->user();
            $token = $user->createToken('auth_token')->plainTextToken;

            return response()->json([
                'user' => $user,
                'message' => 'Successfully Login!',
                'token' => $token,
                'status' => 200
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 500,
                'message' => 'Something went wrong! Please try again.'
            ], 500);
        }
    }



    public function logout(Request $request)
    {
        // return "loggg";
        $request->user()->currentAccessToken()->delete();

        return response()->json(['message' => 'Logged out', 'status' => 200]);
    }

    public function forgetPassword(Request $request)
    {
        $request->validate(['email' => 'required|email']);

        // $status = 'email sent';
        $status = Password::sendResetLink(
            $request->only('email')
        );

        // return $status;
        return $status === Password::RESET_LINK_SENT
            ? response()->json(
                [
                    'message' => 'Password reset link sent to email!',
                    'status' => 200
                ],
                201
            )
            : response()->json(
                [
                    'message' => 'Email not found!',
                    'status' => 500
                ],
                201
            );
    }
}
